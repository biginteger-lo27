/* Copyright (c) 2008 Jérémie Laval, Marine Jourdain

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "biginteger.h"
#include "biginteger_utils.h"


// Uses Karatsuba algorithm
BigInteger mulBigInt(BigInteger x, BigInteger y)
{
  if (x.Sign == 0 || y.Sign == 0)
	return CreateZeroBigInt();

  // If we have a biginteger with one node no point in further splitting
  /* We do an operation like (in base 10) :
   *       abcd
   *       x  e
   *       ----
   * a*e*10^3 + b*e*10^2 + c*e*10 + d*e
   *
   */
  if (MIN(x.Count, y.Count) == 1) {
	BigUnsignedInteger* big = (x.Count == 1) ? y.AbsValStart : x.AbsValStart;
	unsigned int multiplier = (x.Count == 1) ? x.AbsValStart->Value : y.AbsValStart->Value;

	BigInteger temp = mulBigIntByScalar(big, multiplier);
	temp.Sign = x.Sign * y.Sign;

	return temp;
  }

  BigInteger x0 = CreateZeroBigInt();
  BigInteger x1 = CreateZeroBigInt();

  BigInteger y0 = CreateZeroBigInt();
  BigInteger y1 = CreateZeroBigInt();

  int N = MAX(x.Count, y.Count);
  SplitBigInteger(N, x, &x1, &x0);
  SplitBigInteger(N, y, &y1, &y0);
  
  /* a = x1*y1
   * b = x0*y0
   * c = (x1 - x0)(y1 - y0)
   */
  BigInteger a = mulBigInt(x1, y1);
  BigInteger b = mulBigInt(x0, y0);
  BigInteger c = mulBigInt(diffBigInt(x1, x0), diffBigInt(y1, y0));
  BigInteger abc = sumBigInt(a, b);
  abc = diffBigInt(abc, c);

  int k = (N % 2) == 0 ? N/2 : N/2 + 1;
  MultiplyByBase(&a, 2*k);
  MultiplyByBase(&abc, k);

  BigInteger result = sumBigInt(sumBigInt(a, abc), b);
  // Remove leading 0s if any
  Clean(&result);
  result.Sign = x.Sign * y.Sign;

  return result;
}

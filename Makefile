all: biginteger.so

test: biginteger.so
	gcc -Wall -lbiginteger -L. -o test tests/test.c

run: test
	./test 123456 111111

debug: biginteger.c biginteger.h tests/test.c
	gcc -o test -Wall -ggdb *.c *.h tests/test.c

biginteger.so: biginteger.c biginteger.h
	gcc -shared -fpic -Wall -o libbiginteger.so *.c *.h

clean:
	rm test libbiginteger.so
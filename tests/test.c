#include <stdio.h>
#include <stdlib.h>

#include "../biginteger.h"

int main(int argc, char* argv[])
{
  if (argc < 2) {
	puts("Bad arguments");
	return EXIT_FAILURE;
  }

  BigInteger b = newBigInteger(argv[1]);
  BigInteger b2 = newBigInteger(argv[2]);
  
  //printBigInteger(b);

  //printf("Comparing to itself : %d\n", compareBigInt(b, b));

  //printf("Comparing to second : %d\n", compareBigInt(b, b2));

  //printf("Adding the two together : ");

  /*printBigInteger(sumBigInt(b, b2));
  printf("Diff the two :");
  printBigInteger(diffBigInt(b, b2));

  printf("Multiplication : ");
  printBigInteger(mulBigInt(b, b2));*/

  printf("Division : ");
  printBigInteger(quotientBigInt(b, b2));

  //printf("Rest : ");
  //printBigInteger(restBigInt(b, b2));

  return EXIT_SUCCESS;
}

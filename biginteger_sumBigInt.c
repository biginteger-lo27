/* Copyright (c) 2008 Jérémie Laval, Marine Jourdain

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "biginteger.h"
#include "biginteger_utils.h"


BigInteger sumBigInt(BigInteger b1, BigInteger b2)
{
  //test if b2 is negative
  if(b1.Sign==Positive && b2.Sign==Negative){
    b2.Sign=Positive;
    return diffBigInt(b1,b2);
  }else{
    //test if b1 is negative
    if(b1.Sign==Negative && b2.Sign==Positive){
      b1.Sign=Positive;
      return diffBigInt(b2,b1);
    }
  }
  //variable which will be the result of the sum
  BigInteger Sum = CreateZeroBigInt();
  //temporary pointer  over b1
  BigUnsignedInteger* p1 = b1.AbsValStart;
  //temporary pointer over b2
  BigUnsignedInteger* p2 = b2.AbsValStart;
  //test if the two BigInteger are null
  
  if (b1.Sign == Undefined && b2.Sign == Undefined){
    return Sum;
  }else{
    //test if one of the 2 BigInt is null
    if (b1.Sign == Undefined && b2.Sign != Undefined)
      Sum = b2;
    else if (b2.Sign == Undefined && b1.Sign != Undefined)
        Sum = b1;
    
  }
  
  //test if the 2 BigInt are both positive or both negative
  int carry=0;
  //do the addition until we reach the last node of one of the two numbers, or both
  while(p1 != NULL || p2 != NULL){
    unsigned int s=(p1==NULL ? 0 : p1->Value) + (p2==NULL ? 0 : p2->Value) + carry;
    PushEndNode(&Sum, s%10000);
    //compute the carry
    if(s >= 10000)
      //if the sum exceed 10000
      carry=1;
    else
      carry=0;
    
    if (p1 == NULL) {//test if p2 is longer than p1
      p2=p2->Next;
    }else if (p2==NULL) { //test if p1 is longer than p2
      p1=p1->Next;
    }   
  }
  //test if it remain a carry after all the additions
  if(carry==1){
    PushEndNode(&Sum,1);
  }
  //if the two numbers are both negative
  if(b1.Sign == Negative && b2.Sign == Negative)
      Sum.Sign=Negative;
 
  return Sum;

}

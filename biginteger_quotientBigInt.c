/* Copyright (c) 2008 Jérémie Laval, Marine Jourdain

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "biginteger.h"
#include "biginteger_utils.h"

// Uses a version of Knuth LongDivision algorithm found in the Art of Computer Programming vol2
BigInteger quotientBigInt(BigInteger u, BigInteger v)
{
  // Special cases due to comparison
  int comparison = compareBigInt(u, v);
  
  // Since we are only interested in integer number if
  // b is greater than a the result is automatically rounded to 0
  if (comparison <= 0)
	return comparison < 0 ? CreateZeroBigInt() : newBigInteger("1");
  
  BigInteger quotient = CreateZeroBigInt();
  quotient.Sign = (Sign)(u.Sign * v.Sign);

  // Special case of u.Count == 1 and v.Count == 1
  if (u.Count == 1 && v.Count == 1) {
	unsigned int result =  u.AbsValStart->Value / v.AbsValStart->Value;
	PushFrontNode(&quotient, result);
	return quotient;
  }

  // Special case of u.Count > 1 and v.Count == 1
  if (v.Count == 1) {
	unsigned int r = 0;
	BigUnsignedInteger* unode = u.AbsValEnd;
	int j = 1;
	
	for (; j <= u.Count; j++) {
	  PushFrontNode(&quotient, (r * BASE + unode->Value) / v.AbsValEnd->Value);
	  r = (r * BASE + unode->Value) % v.AbsValEnd->Value;
	  unode = unode->Previous;
	}
	Clean(&quotient);

	return quotient;
  }

  // Base case
  
  unsigned int divisor = BASE / (v.AbsValEnd->Value + 1);

  BigInteger uprime = mulBigIntByScalar(u.AbsValStart, divisor);
  BigInteger vprime = mulBigIntByScalar(v.AbsValStart, divisor);
  
  // Normalize uprime
  if (uprime.Count == u.Count)
	//if (divisor == 1)
	PushEndNode(&uprime, 0);
  // Normalize vprime (unecessary due to how d is computed)
  if (vprime.Count > v.Count)
	// It's only a supplementary node
	PopEndNode(&vprime);


  unsigned int v1 = vprime.AbsValEnd->Value;
  unsigned int v2 = vprime.AbsValEnd->Previous->Value;

  // Used to get sub-biginteger of uprime in the loop
  int j = 0;
  int m = u.Count - v.Count;
  int n = v.Count;

  BigUnsignedInteger* ujNode = uprime.AbsValEnd;

  for (; j <= m; j++) {
	unsigned int q = BASE - 1;
	unsigned int uj = ujNode->Value;

	if (uj != v1)
	  q = (uj * BASE + ujNode->Previous->Value) / v1;

	// Overflow test
	while (v2 * q > (uj * BASE + ujNode->Previous->Value - q * v1) * BASE + ujNode->Previous->Previous->Value)
	  q -= 1;

	BigInteger slice = CreateZeroBigInt();
	slice.Sign = Positive;
	CopyBigIntegerFromEnd(uprime, &slice, ujNode, n + 1);

	BigInteger qv = mulBigIntByScalar(vprime.AbsValStart, q);
	slice = diffBigInt(slice, qv);
	
	ujNode = ReplaceFromEnd(&uprime, slice, ujNode->Next, n);
	
	PushFrontNode(&quotient, q);
  }
  
  Clean(&quotient);

  return quotient;
}

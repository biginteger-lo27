/* Copyright (c) 2008 Jérémie Laval, Marine Jourdain

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef _BIG_INTEGER_H
#define _BIG_INTEGER_H

// Type declaration

#define BASE 10000

typedef struct _BigUnsignedInteger {
  // A coefficient in base 10,000
  unsigned int                Value;
  // Next coefficient
  struct _BigUnsignedInteger* Next;
  // Previous coefficient
  struct _BigUnsignedInteger* Previous;

} BigUnsignedInteger;


typedef enum {
  Negative = -1,
  Positive = 1,
  Undefined = 0

} Sign;

typedef struct {
  // Restricted to value -1, 0 or 1
  Sign                Sign;
  // The absolute value which is multiplied by sign to get the real integer value
  BigUnsignedInteger* AbsValStart; // Head
  BigUnsignedInteger* AbsValEnd; // Tail
  int Count;
} BigInteger;


typedef enum {
  false = 0,
  true = 1
} Boolean;

// End type declaration

// Public Functions declaration

BigInteger newBigInteger(char*);

void printBigInteger(BigInteger);

Boolean isNull(BigInteger);

int signBigInt(BigInteger);

Boolean equalsBigInt(BigInteger, BigInteger);

int compareBigInt(BigInteger, BigInteger);

BigInteger sumBigInt(BigInteger, BigInteger);

BigInteger diffBigInt(BigInteger, BigInteger);

BigInteger mulBigInt(BigInteger, BigInteger);

BigInteger quotientBigInt(BigInteger, BigInteger);

BigInteger restBigInt(BigInteger, BigInteger);

BigInteger gcdBigInt(BigInteger, BigInteger);

BigInteger lcmBigInt(BigInteger, BigInteger);

BigInteger factorial(unsigned long);

BigInteger cnp(unsigned long, unsigned long);

// End Public Function Declaration

#endif

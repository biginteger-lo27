/* Copyright (c) 2008 Jérémie Laval, Marine Jourdain

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdlib.h>
#include "biginteger.h"

#ifndef _BIG_INTEGER_UTILS_H
#define _BIG_INTEGER_UTILS_H

// Macros

#define ATOUI(str) ((unsigned int)strtoul(buffer, NULL, 10))
#define MIN(x, y) (x < y ? x : y)
#define MAX(x, y) (x > y ? x : y)

// Private Function Declaration

BigUnsignedInteger* ReturnNthNode(BigInteger*, int);

BigInteger CreateZeroBigInt();

BigUnsignedInteger* InitBUint(unsigned int);

BigUnsignedInteger* CopyBigInteger(BigInteger source, BigInteger* dest, BigUnsignedInteger* start, int count);

BigUnsignedInteger* CopyBigIntegerFromEnd(BigInteger source, BigInteger* dest, BigUnsignedInteger* start, int count);

BigUnsignedInteger* ReplaceFromEnd(BigInteger* source, BigInteger slice, BigUnsignedInteger* start, int count);

void Clean(BigInteger* b);

void SplitBigInteger(int N, BigInteger base, BigInteger* part1, BigInteger* part2);

void MultiplyByBase(BigInteger* b, int k);

void PrependNode(BigUnsignedInteger*, BigUnsignedInteger*);

void PushFrontNode(BigInteger*, unsigned int);

void PushEndNode(BigInteger*, unsigned int);

void PopFrontNode(BigInteger*);

void PopEndNode(BigInteger*);

void InsertNode(BigInteger*, BigUnsignedInteger*, unsigned int);

unsigned int GetValueAtFromEnd(BigInteger b, int i);

BigInteger mulBigIntByScalar(BigUnsignedInteger* big, unsigned int multiplier);

BigInteger powBigInt(BigInteger b, int n);

// End Private Function Declaration

#endif

/* Copyright (c) 2008 Jérémie Laval, Marine Jourdain

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "biginteger.h"
#include "biginteger_utils.h"


BigInteger diffBigInt(BigInteger b1, BigInteger b2)
{
  // Sign cases
  // -b1 - (+b2) <=> -(b1 + b2)
  if (b1.Sign == Negative && b2.Sign == Positive) {
	b2.Sign = Negative;
	return sumBigInt(b1, b2);
  }

  // b1 - (-b2) <=> b1 + b2
  if (b1.Sign == Positive && b2.Sign == Negative) {
	b2.Sign = Positive;
	return sumBigInt(b1, b2);
  }

  BigInteger result = CreateZeroBigInt();

  BigUnsignedInteger* node1 = b1.AbsValStart;
  BigUnsignedInteger* node2 = b2.AbsValStart;
  
  // NULL case
  if (node2 == NULL)
	return b1;
  if (node1 == NULL) {
	b2.Sign = b2.Sign == Positive ? Negative : Positive;
	return b2;
  }  

  int comp = compareBigInt(b1, b2);
  if (comp == 0) {
	return result;
  } else if (comp > 0) {
	result.Sign = Positive;
  } else {
	// Some magic swapping
	result.Sign = Negative;
	BigUnsignedInteger* temp = node1;
	node1 = node2;
	node2 = temp;
  }

  // Takes back the pointer value as there might have been swapping
  // TODO : see if we can exchange directly these pointer in the if rather than b1/b2
  /*node1 = b1.AbsValStart;
  node2 = b2.AbsValStart;
  */

  unsigned int carry = 0;
  Boolean cont = false;

  do {
	cont = false;

	unsigned int newValue = 0;
	if (node1 != NULL && node2 != NULL) {
	  if (node2->Value > node1->Value) {
		// Do a little trick to get positive coefficient
		// and backport the changement on the upper coefficient with carry
		newValue = (BASE + node1->Value) - node2->Value - carry;
		carry = 1;
	  } else {
		newValue = node1->Value - node2->Value - carry;
		carry = 0;
	  }
	} else {
	  // Due to our hypothethis at start of the function (test, swapping and all)
	  // we know that it's node2 which is NULL, then proceed with node1
	  newValue = node1->Value - carry;
	  carry = 0;
	  // No need for unnecessary 0 in the list
	  if (newValue == 0)
		break;
	}

	PushEndNode(&result, newValue);

	if (node1 != NULL) {
	  cont = (node1 = node1->Next) != NULL;
	}
	if (node2 != NULL) {
	  node2 = node2->Next;
	  cont = node2 != NULL || cont;
	}

  } while(cont);

  return result;
}

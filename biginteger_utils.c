/* Copyright (c) 2008 Jérémie Laval, Marine Jourdain

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "biginteger.h"
#include "biginteger_utils.h"

// Private Function Impl

BigInteger CreateZeroBigInt()
{
  BigInteger temp;
  temp.Sign = Undefined;
  temp.AbsValStart = NULL;
  temp.AbsValEnd = NULL;
  temp.Count = 0;

  return temp;
}

BigUnsignedInteger* InitBUint(unsigned int value)
{
  BigUnsignedInteger* bigUint = malloc(sizeof(BigUnsignedInteger));
  bigUint->Value = value;
  
  return bigUint;
}

BigUnsignedInteger* CopyBigInteger(BigInteger source, BigInteger* dest, BigUnsignedInteger* start, int count)
{
  BigUnsignedInteger* node = (start == NULL) ? source.AbsValStart : start;

  do {
	PushEndNode(dest, node->Value);
	if (--count == 0)
	  break;
  } while ((node = node->Next) != NULL);

  return node;
}

BigUnsignedInteger* CopyBigIntegerFromEnd(BigInteger source, BigInteger* dest, BigUnsignedInteger* start, int count)
{
  BigUnsignedInteger* node = (start == NULL) ? source.AbsValEnd : start;

  do {
	PushFrontNode(dest, node->Value);
	if (--count == 0)
	  break;
  } while ((node = node->Previous) != NULL);

  return node;
}

BigUnsignedInteger* ReplaceFromEnd(BigInteger* source, BigInteger slice, BigUnsignedInteger* start, int count)
{
  int i = 0;

  if (start == NULL) {
	start = source->AbsValEnd;
	source->AbsValEnd = slice.AbsValEnd;
	i = 1;
  }	else {
	/*start->Previous = slice.AbsValEnd;
	  slice.AbsValEnd->Next = start;*/
	PrependNode(slice.AbsValEnd, start);
  }  

  BigUnsignedInteger* endNode = start;

  for (; i <= count && endNode != NULL; i++)
	endNode = endNode->Previous;

  printf("endNode is null %d\n", endNode == NULL);
  if (endNode != NULL) {
	/*endNode->Next = slice.AbsValStart;
	  slice.AbsValStart->Previous = endNode;*/
	PrependNode(endNode, slice.AbsValStart);
  } else {
	source->AbsValStart = slice.AbsValStart;
  }

  source->Count -= count;
  source->Count += slice.Count;

  return slice.AbsValEnd->Previous;
}

void Clean(BigInteger* b)
{
  BigUnsignedInteger* node = b->AbsValEnd;
  if (node->Value != 0)
	return;

  while (node->Value == 0) {
	BigUnsignedInteger* toFree = node;
	node = node->Previous;
	free(toFree);
	b->Count--;
  }
  
  // Erase things up
  b->AbsValEnd = node;
  node->Next = NULL;
}

// base = part1 * 10000^N  + part2
void SplitBigInteger(int N, BigInteger base, BigInteger* part1, BigInteger* part2)
{
  // In case N is odd part2 has a 1 more node than part1
  int count = (N % 2) == 0 ? N/2 : N/2 + 1;

  if (base.Count <= count) {
	// Only set part2, part1 == 0
	CopyBigInteger(base, part2, NULL, -1);
	part2->Sign = Positive;
	return;
  }
  
  BigUnsignedInteger* node = CopyBigInteger(base, part2, NULL, count);
  part2->Sign = Positive;

  CopyBigInteger(base, part1, node->Next, -1);
  part1->Sign = Positive;

}

void MultiplyByBase(BigInteger* b, int k)
{
  int i = 0;
  for (; i < k; i++)
	PushFrontNode(b, 0);
}

void PrependNode(BigUnsignedInteger* b1, BigUnsignedInteger* b2)
{
  b1->Next = b2;
  b2->Previous = b1;
}

// Used to push_front a value
void PushFrontNode(BigInteger* bigInt, unsigned int value)
{
  BigUnsignedInteger* bigUint = InitBUint(value);
  
  // Do the magic swapping
  if (bigInt->AbsValStart != NULL)
	PrependNode(bigUint, bigInt->AbsValStart);
  // Update BigInt's head field
  bigInt->AbsValStart = bigUint;
  
  // If it's the first node added, correctly set bigInt->AbsValEnd
  if (bigInt->AbsValEnd == NULL)
	bigInt->AbsValEnd = bigUint;

  bigInt->Count++;
}

void PushEndNode(BigInteger* bigInt, unsigned int value)
{
  BigUnsignedInteger* buint = InitBUint(value);
  
  // Do the magic swapping
  if (bigInt->AbsValEnd != NULL)
	PrependNode(bigInt->AbsValEnd, buint);
  // Update BigInt's head field
  bigInt->AbsValEnd = buint;
  
  // If it's the first node added, correctly set bigInt->AbsValStart
  if (bigInt->AbsValStart == NULL)
	bigInt->AbsValStart = buint;

  bigInt->Count++;
}

void PopFrontNode(BigInteger* bigInt)
{
  bigInt->Count--;

  BigUnsignedInteger* node = bigInt->AbsValStart;
  node->Next->Previous = NULL;
  bigInt->AbsValStart = node->Next;

  free(node);    
}

void PopEndNode(BigInteger* bigInt)
{
  bigInt->Count--;

  BigUnsignedInteger* node = bigInt->AbsValEnd;
  node->Previous->Next = NULL;
  bigInt->AbsValEnd= node->Previous;

  free(node);    
}


// Used to insert a node in the *middle* of the BigInt.
// It Do *not* check if we reproduce Push(Front|End)Node behavior
// Use Push(Front|End)Node if you intend to do what these functions are meant for
void InsertNode(BigInteger* bigInt, BigUnsignedInteger* next, unsigned int value)
{
  BigUnsignedInteger* buint = InitBUint(value);
  BigUnsignedInteger* temp  = next->Previous;

  PrependNode(buint, next);
  PrependNode(temp, buint);

  bigInt->Count++;
}

unsigned int GetValueAtFromEnd(BigInteger b, int index)
{
  BigUnsignedInteger* node = b.AbsValEnd;
  
  int i = 0;
  for (; i < index; i++) {
	if (node == NULL)
	  return 0;
	node = node->Previous;
  }
  return node->Value;
}

BigInteger mulBigIntByScalar(BigUnsignedInteger* big, unsigned int multiplier)
{
  BigInteger temp = CreateZeroBigInt();
  unsigned int carry = 0;

  // Multiplied bigint supposed to be positive
  temp.Sign = multiplier < 0 ? Negative : Positive;
  
  while (big != NULL) {
	unsigned int r = big->Value * multiplier + carry;
	carry = r / BASE;
	PushEndNode(&temp, r % BASE);
	
	big = big->Next;
  }
  if (carry != 0)
	PushEndNode(&temp, carry);
  
  return temp;
}

// Returns b^n
BigInteger powBigInt(BigInteger b, int n)
{
  BigInteger result = b;

  while ((n & 0x1) == 0) {
	result = mulBigInt(result, result);
	n /= 2;
  }
  if (n != 1) {
	int i = n;
	for (; i >= 1; i--)
	  result = mulBigInt(result, b);
  }

  return result;
}


// End Private Function Impl

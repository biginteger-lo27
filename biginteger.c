/* Copyright (c) 2008 Jérémie Laval, Marine Jourdain

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "biginteger.h"
#include "biginteger_utils.h"

// Public Function Impl

BigInteger newBigInteger(char* desc)
{
  BigInteger   newbigint=CreateZeroBigInt();
  //iterative variable
  int          i;
  //array of character where is stored a part of the total list of character
  char         temp[5];
  //temporary variable where is stored the integer incoming from the transformation 'character to interger'
  unsigned int tempint;
  
  //loop to build the double linked list until we reach the last group of 4 characters
  for(i = strlen(desc)-5; i >= 0; i = i - 4) {
	  strncpy(temp, desc + i,4);
	  temp[5] = '\0';
	  tempint = atoi(temp);
	  PushEndNode(&newbigint,tempint);
  }
  
  //test if the number of digit of the number is divisible by 4. if it's not the case, it completes with the rest
  if (i < 0) {
	  strncpy(temp, desc, i + 4);
	  temp[i+5] = '\0';
	  tempint = atoi(temp);
	  PushEndNode(&newbigint, tempint);
  }
  
  return newbigint;
}

void printBigInteger(BigInteger bint)
{
  if (bint.Sign == Negative)
	printf("%c", '-');
  
  BigUnsignedInteger* node = bint.AbsValEnd;

  do {
	if (node != bint.AbsValEnd)
	  printf("%04u", node->Value);
	else
	  printf("%u", node->Value);
  } while((node = node->Previous) != NULL);
  
  printf("\n");
}

Boolean isNull(BigInteger bint)
{
  return false;
}

int signBigInt(BigInteger bint)
{
  return -1;
}

Boolean equalsBigInt(BigInteger b1, BigInteger b2)
{
  return false;
}

/* b1 > b2 => 1
 * b1 < b2 => -1
 * b1 == b2 => 0
 */
int compareBigInt(BigInteger b1, BigInteger b2)
{ 
  //trivial cases
  //test if only one of the two big integer is negative
  if (b1.Sign == Positive && b2.Sign == Negative) 
    return 1;
  else if (b1.Sign == Negative && b2.Sign == Positive)
    return -1;
  //test if one of the two big int is shorter than the other
  if (b1.Count != b2.Count)
    return b1.Count > b2.Count ? 1 : -1;
  //if the two big int are null
  if(b1.Sign==Undefined && b2.Sign==Undefined)
    return 0;
  //temporary pointers to go through b1 and b2  
  BigUnsignedInteger* end1=b1.AbsValEnd;
  BigUnsignedInteger* end2=b2.AbsValEnd;
  
  //general case
  //look for the first group of 4 digits which is different according the 2 bigint
  while(end1->Value == end2->Value && end1!=NULL){   
    end1 = end1->Previous;
    end2 = end2->Previous;
  }
  
  //test if the 2 bigint are equals (we reach a step where all the nodes before are equals)
  if(end1 == NULL)
    return 0;

  return end1->Value > end2->Value ? 1 : -1;
}

// End Public Function Impl
